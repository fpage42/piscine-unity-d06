﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera : MonoBehaviour {

	public float speedRotation;
	private float pitch = 0.0f;
	private float maxPitch = 90.0f;

	void Start () {
		Cursor.visible = false;
	}
	
	// Update is called once per frame
	void Update () {
		pitch -= this.speedRotation * Input.GetAxis("Mouse Y");
		if (this.pitch >= this.maxPitch)
			this.pitch = this.maxPitch;
		else if (this.pitch <= -this.maxPitch)
			this.pitch = -this.maxPitch;
		transform.eulerAngles = new Vector3(pitch, transform.eulerAngles.y, 0.0f);
	}
}
