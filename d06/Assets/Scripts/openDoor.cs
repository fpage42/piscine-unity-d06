﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class openDoor : MonoBehaviour {

	public GameObject door;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerStay(Collider col)
	{
		if (col.GetComponent<Character>() && col.gameObject.GetComponent<Character>().hasCard() && Input.GetKeyDown (KeyCode.E))
			door.SetActive (false);
	}
}
