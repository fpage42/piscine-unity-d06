﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProgressBar : MonoBehaviour {


	float sizeX;
	float posX;
    public float centerMin;
	public Character player;
    public Sprite Alerte;
    public Sprite normal;
	public GameObject panAlarme;

	// Use this for initialization
	void Start () {
		this.sizeX = this.gameObject.transform.localScale.x;
		this.posX = this.gameObject.transform.position.x;
		this.gameObject.transform.localScale = new Vector3(0.0f, this.transform.localScale.y, this.transform.localScale.z);
   }
	
	void Update () {
             if (((float)this.player.getReperage() / this.player.maxReperage) > 0.75f)
             {
				this.gameObject.GetComponent<Image>().sprite = Alerte;
				panAlarme.SetActive (true);
             }
             else
             {
				this.gameObject.GetComponent<Image>().sprite = normal;
				panAlarme.SetActive (false);
			}
	    this.gameObject.transform.localScale = new Vector3(this.sizeX / this.player.maxReperage * this.player.getReperage(), this.gameObject.transform.localScale.y, this.gameObject.transform.localScale.z);
		this.gameObject.transform.localPosition = new Vector3 (centerMin * (((float)this.player.getReperage () / this.player.maxReperage)-1), this.transform.localPosition.y, this.transform.localPosition.z);
	}
}
	