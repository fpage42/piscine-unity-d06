﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Document : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerStay(Collider col)
	{
		if (col.GetComponent<Character> () && Input.GetKeyDown (KeyCode.E)) {
			SceneManager.LoadScene (SceneManager.GetActiveScene ().name);
		}
	}
}
