﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Card : MonoBehaviour {

	GameObject canTake = null;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (canTake && Input.GetKeyDown (KeyCode.E)) {
			canTake.GetComponent<Character> ().takeCard();
			this.gameObject.SetActive (false);
		}
	}

	void OnTriggerEnter(Collider col)
	{
		if (col.gameObject.GetComponent<Character> ())
			canTake = col.gameObject;
	}

	void OnTriggerExit(Collider col)
	{
		if (col.gameObject.GetComponent<Character> ())
			canTake = null;
	}
}
