﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class Character : MonoBehaviour {

	private float yaw = 0.0f;
	public float speedRotation;
	public float speed;
	private int reperage = 0;
	public int maxReperage;

	private bool card = false;

	void Start () {

    }
	
	// Update is called once per frame
	void Update () {
		CharacterController controller = GetComponent<CharacterController>();
		Vector3 move = new Vector3 (0, 0, 0);
        if (Input.GetKey(KeyCode.W))
			move += (this.transform.forward * speed);
        if (Input.GetKey(KeyCode.S))
			move += (-this.transform.forward * speed);
        if (Input.GetKey(KeyCode.A))
			move += (-this.transform.right * speed);
        if (Input.GetKey(KeyCode.D))
			move += (this.transform.right * speed);
		controller.SimpleMove (move);

		yaw += this.speedRotation * Input.GetAxis("Mouse X");
		transform.eulerAngles = new Vector3(transform.eulerAngles.x, yaw, 0.0f);
		if (reperage > 0)
			reperage -= 1;
	}

	public void addReperage(int power)
	{
		if (reperage + power > maxReperage)
			reperage = maxReperage;
		else
			reperage += power;
		if (reperage >= maxReperage) {
			Debug.Log ("lose");
			SceneManager.LoadScene (SceneManager.GetActiveScene ().name);
		}
	}

	public void takeCard()
	{
		card = true;
	}

	public bool hasCard()
	{
		return this.card;
	}

	public int getReperage()
	{
		return this.reperage;
	}
}
