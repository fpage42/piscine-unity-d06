﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraLight : MonoBehaviour {

	public int power;
    private bool fan = false;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerStay(Collider col)
	{
		Character charact = col.gameObject.GetComponent<Character> ();
		if (charact != null) {
			charact.addReperage (power);
		}
	}

    public void activeFan()
    {
        if (!fan)
        {
            this.power--;
            fan = true;
        }
    }
}
