﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fan : MonoBehaviour {

    public CameraLight camera;
    public GameObject smoke;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerStay(Collider col)
    {
        if (col.gameObject.GetComponent<Character>() && Input.GetKeyDown(KeyCode.E))
        {
            camera.activeFan();
            smoke.SetActive(true);
        }
    }
}
